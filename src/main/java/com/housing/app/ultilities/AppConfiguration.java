package com.housing.app.ultilities;

import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Configuration;

@Configuration
@PropertySource("classpath:appConfig.properties")
public class AppConfiguration {

}
